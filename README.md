Village analytics
---

## Database setup

1. Download data from <https://drive.google.com/file/d/0Byzm9OnoiDKYM2JnX0FQUi0yX3M/view> and <https://drive.google.com/file/d/0Byzm9OnoiDKYNnNFTHkwNVpGQVU/view> and extract them into `data/`
2. Then run the below commands

```
$ psql -U postgres -f setup.sql
$ python database.py setup
$ psql -U village # password is village
village=> \copy villages FROM 'data/village_analytics_master.csv' DELIMITER ',' CSV HEADER NULL AS 'NA';
village=> \copy log_villages FROM 'data/village_analytics_master_log.csv' DELIMITER ',' CSV HEADER NULL AS 'NA';
village=> \copy locations FROM 'data/locations.csv' DELIMITER ',' CSV HEADER NULL AS 'NA';
village=> \q
$ python database.py index
```
