'''
The MIT License (MIT)
Copyright (c) 2016 Suhas SG

postgres=# CREATE DATABASE village;
postgres=# CREATE USER village WITH PASSWORD 'village';
postgres=# GRANT ALL PRIVILEGES ON DATABASE village TO village;
'''
import os
import logging
import sqlalchemy
import pandas as pd
from sqlalchemy import Column, Numeric, Text

logging.basicConfig(
    format='%(asctime)s|%(levelname)s|%(message)s',
    level=logging.INFO
)


def connect():
    '''Returns a connection and a metadata object'''
    url = 'postgresql://village:village@localhost:5432/village'
    con = sqlalchemy.create_engine(url, client_encoding='utf8')
    meta = sqlalchemy.MetaData(bind=con, reflect=True)
    return con, meta


def setup(filename, tablename):
    con, meta = connect()
    df = pd.read_csv(filename, nrows=10)

    # All numeric columns
    fields = [
        Column(col, Text)
        if isinstance(df[col].iloc[7], basestring)
        else Column(col, Numeric)
        for col in df.columns
    ]

    # Create table
    sqlalchemy.Table(tablename, meta, *fields, extend_existing=True)

    meta.create_all(con)
    logging.info('Created table %s for %s' % (filename, tablename))


def indexer(filename, tablename):
    con, meta = connect()
    con.execute('ALTER TABLE %s ADD COLUMN country_code numeric DEFAULT 1' % tablename)
    df = pd.read_csv(filename, nrows=10)
    for col in df.columns:
        q = '''
            CREATE INDEX %s_%s_index ON %s ("%s")
        ''' % (col, tablename, tablename, col)
        con.execute(q)
        logging.info(q)


if __name__ == '__main__':
    import sys
    if len(sys.argv) > 1:
        if sys.argv[1] == 'setup':
            setup('data/village_analytics_master.csv', 'villages')
            setup('data/village_analytics_master_log.csv', 'log_villages')
            setup('data/locations.csv', 'locations')
        elif sys.argv[1] == 'index':
            indexer('data/village_analytics_master.csv', 'villages')
            indexer('data/village_analytics_master_log.csv', 'log_villages')
            indexer('data/locations.csv', 'locations')
        else:
            print 'Please provide an argument. Read README.md for instructions.'
    else:
        print 'Please provide an argument. Read README.md for instructions.'
