'''
The MIT License (MIT)
Copyright (c) 2016 Suhas SG
'''
import logging
import StringIO
import pandas as pd
from flask import Flask
from flask import request
from flask import Response
from flask import make_response
from urllib import quote, unquote
from flask import render_template, jsonify
from database import connect
import numpy as np
app = Flask(__name__)

logging.basicConfig(
    format='%(asctime)s|%(levelname)s|%(message)s',
    level=logging.INFO
)

con, meta = connect()
datavars = pd.read_csv('data/variables.csv', encoding='utf-8')


@app.route('/locations')
def locations():
    '''
    /locations?get=district&for=state&id=
    '''
    if 'id' not in request.args:
        g = request.args.get('get')
        q = '''
        SELECT DISTINCT %s_code, %s_name FROM locations
        ''' % (g, g)
    else:
        g, f, i = [request.args.get(a) for a in ['get', 'for', 'id']]
        q = '''
        SELECT DISTINCT %s_code, %s_name FROM locations
        WHERE %s_code = %s
        ''' % (g, g, f, i)
    df = pd.read_sql(q, con)
    output = make_response(df.to_json(orient='records'))
    output.headers["Content-type"] = "application/json"
    output.headers["Access-Control-Allow-Origin"] = "*"
    return output


@app.route('/api')
def data():
    '''
    /api?village=&metric=&subset=country|state|district
    ---
        {
            "perc": N,
            "data": [
                {"x": "Xi (log(X))", "y": "Yi (freq)"},
                ...
            ]
        }
    '''

    # step 1: get village code and metric passed from api
    village_code, metric = request.args.get('village'), request.args.get('metric')

    # step 2: get the country / state / district code for the above village
    # based on the subset
    subset = request.args.get('subset')
    q = 'SELECT %s_code FROM villages WHERE village_code = %s' % (subset, village_code)
    r = con.execute(q).fetchone()
    if not r:
        return jsonify(**{})
    f = int(r[0])

    # step 3: compute percentage rank and retrieve
    q = '''
    SELECT percent_rank
    FROM (
        SELECT village_code, "%s", percent_rank()
        OVER (ORDER BY "%s" ASC)
        FROM villages
        WHERE %s_code = %s
    ) sub_query_1
    WHERE village_code = %s
    ''' % (metric, metric, subset, f, village_code)
    perc = float(con.execute(q).fetchone()[0])

    # step 4: compute xs, ys on log scale
    q = '''
    SELECT "log_%s", count("log_%s") FROM log_villages
    WHERE %s_code = %s
    GROUP BY "log_%s"
    ''' % (metric, metric, subset, f, metric)
    result = pd.read_sql(q, con)

    # step 5: compute position of village on x axis
    q = '''
    SELECT "log_%s" FROM log_villages WHERE village_code = %s
    ''' % (metric, village_code)
    pos = float(con.execute(q).fetchone()[0])

    result.columns = ['x', 'y']
    result.fillna(0, inplace=True)
    bins = np.linspace(result.x.min(), result.x.max(), 10)
    groups = result.groupby(np.digitize(result.x, bins))
    result = groups.agg({'y': sum, 'x': np.mean})
    result['x'] = result['x'].astype(float)
    result['y'] = result['y'].astype(int)
    result = result.sort_values(by='x')
    xys = result.to_dict(orient='records')
    if 'plotlyjs' in request.args:
        xys = result.to_dict()

    resp = {
        'pos': pos,
        'data': xys,
        'perc': perc,
        'subset': subset,
        'metric': metric,
        'village_code': village_code
    }

    print resp

    return jsonify(**resp)


@app.route('/')
def explore():
    return render_template('index.html', datavars=datavars)


@app.route('/download')
def download():
    village_code = request.args.get('village')
    q = '''
    SELECT * FROM villages WHERE village_code = %s
    ''' % village_code
    df = pd.read_sql(q, con)
    si = StringIO.StringIO()
    df.T.to_csv(si)
    output = make_response(si.getvalue())
    output.headers["Content-Disposition"] = "attachment; filename=village-%s.csv" % village_code
    output.headers["Content-type"] = "text/csv"
    return output


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=7000, debug=True, threaded=True)
